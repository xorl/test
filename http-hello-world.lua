local Http = require('http')

local body = "Hello World\n"

Http.create_server("0.0.0.0", 8080, function (req, res)
  res:write_head(200, {
    ["Content-Type"] = "text/plain",
    ["Content-Length"] = #body
  })
  res:finish(body)
end)

print("HTTP hello world listening at http://localhost:8080/")
