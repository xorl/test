local Http = require 'http'

local PORT = process.env.PORT or 8080

local server = Http.create_server(function (req, res)
  res:finish("Hello\n")
end)

server:listen(PORT)

print("Server listening on http://localhost:" .. PORT .. "/")
