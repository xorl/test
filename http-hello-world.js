var Http = require('http');

var body = "Hello World\n";

Http.createServer(function (req, res) {
  res.writeHead(200, {
    "Content-Type": "text/plain",
    "Content-Length": Buffer.byteLength(body)
  });
  res.end(body);
}).listen(8081);

console.log("HTTP hello world listening at http://localhost:8081/");
