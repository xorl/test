local SDL = require 'sdl'
local Events = require 'events'
local FFI = require 'ffi'
local Table = require 'table'
local Math = require 'math'

SDL.SDL_Init(SDL.SDL_INIT_VIDEO)
SDL.SDL_WM_SetCaption("Luvit Chaser!", nil);
local screen = SDL.SDL_SetVideoMode(0, 0, 0, SDL.SDL_FULLSCREEN)

SDL.SDL_InitSubSystem(SDL.SDL_INIT_JOYSTICK)
local joysticks = {}
SDL.SDL_JoystickEventState(SDL.SDL_ENABLE);
p("numjoysticks", SDL.SDL_NumJoysticks())
for i = 1,SDL.SDL_NumJoysticks() do
  p("Found ", FFI.string(SDL.SDL_JoystickName(i-1)));
  local joy = SDL.SDL_JoystickOpen(i-1)
  joysticks[i] = joy
end
p("Joysticks", joysticks)


local function exit()
  Events.stop_events()
  SDL.SDL_Quit()
  print("Thanks for Playing!")
end

Events:on(SDL.SDL_QUIT, exit)
Events:on(SDL.SDL_KEYDOWN, function (evt)
  local k = evt.key
  local sym = k.keysym.sym
  if sym == SDL.SDLK_ESCAPE then
    exit()
  end
end)

local Rect = FFI.metatype("SDL_Rect", {})


local colors = {
  {0xfce94f, 0xedd400, 0xc4a000},
  {0x8ae234, 0x73d216, 0x4e9a06},
  {0xe9b96e, 0xc17d11, 0x8f5902},
  {0xfcaf3e, 0xf57900, 0xce5c00},
  {0xad7fa8, 0x75507b, 0x5c3566},
  {0xef2929, 0xcc0000, 0xa40000},
  {0x729fcf, 0x3465a4, 0x204a87},
}

local color_index = 1

local box

local sprites = {}
local off = 0
local function explosion(x, y, color_set)
  local life = 0
  off = (off + 1) % 21
  local start = off * 3 % 13
  local index = off % 3 + 1
  Table.insert(sprites, function (delta)
    for i = start, 360,3 do
      local px = x + Math.sin(i / 180 * Math.pi) * life
      local py = y + Math.cos(i / 180 * Math.pi) * life
      SDL.SDL_FillRect(screen, Rect(px - 5, py - 5, 10, 10), color_set[index])
    end
    life = life + delta
    return life > 1000
  end)
end

function box(x, y, color_set, scale)
  scale = scale or 1
  local life = 0
  Table.insert(sprites, function (delta)
    SDL.SDL_FillRect(screen, Rect(x - 10 * scale, y - 10 * scale, 20 * scale, 20 * scale), color_set[3])
    SDL.SDL_FillRect(screen, Rect(x - 6 * scale, y - 6 * scale, 12 * scale, 12 * scale), color_set[2])
    SDL.SDL_FillRect(screen, Rect(x - 3 * scale, y - 3 * scale, 6 * scale, 6 * scale), color_set[1])
    life = life + delta
    return life > 500 / scale
  end)
end

Events:on(SDL.SDL_MOUSEMOTION, function (evt)
  local m = evt.motion
  if m.state == 1 then
--    explosion(m.x, m.y, colors[color_index])
    box(m.x, m.y, colors[color_index])
  end
end)

Events:on(SDL.SDL_MOUSEBUTTONDOWN, function (evt)
  local b = evt.button
  color_index = (color_index % #colors) + 1
  box(b.x, b.y, colors[color_index], 2)
  explosion(b.x, b.y, colors[color_index])
end)

Events:on(SDL.SDL_MOUSEBUTTONUP, function (evt)
  local b = evt.button
  explosion(b.x, b.y, colors[color_index])
  box(b.x, b.y, colors[color_index], 2)
end)

local jx = {}
local jy = {}
Events:on(SDL.SDL_JOYAXISMOTION, function (evt)
  local j = evt.jaxis
  local index = Math.floor(j.axis / 2) + 1
  local value = j.value / 65536 + 0.5
  if j.axis % 2 == 0 then
    jx[index] = Math.floor(screen.w * value)
  elseif j.axis % 2 == 1 then
    jy[index] = Math.floor(screen.h * value)
  end
  if jx[index] and jy[index] then
    explosion(jx[index], jy[index], colors[index % #colors + 1])
  end
end)


Events:on('tick', function (delta)
  SDL.SDL_FillRect(screen, nil, 0x000000);
--  for i=1,100 do
--    SDL.SDL_FillRect(screen, Rect(Math.random(0, screen.w), Math.random(0, screen.h), 2, 2), 0)
--  end
  for i = #sprites, 1, -1 do
    if sprites[i](delta) then
      Table.remove(sprites, i)
    end
  end
  SDL.SDL_Flip(screen);
end)



